const io=require("../io")
const requestJson=require("request-json")

const baseMlabURL="https://api.mlab.com/api/1/databases/apitechujmfg13ed/collections/"
const apiKey="apiKey=5zTFmFoXB8txjvfwTLCXt6mCqPL-P_ea";

function getUsersV1(req,res){
  console.log("GET /apitechu/v1/users");
  var users=require('../usuarios.json');
  var result={"users":users};
  if(req.query.$count){
    var len=users.length;
    result['count']=len;
  }
  if(req.query.$top){
    users=users.slice(0,req.query.$top);
    result['users']=users;
  }
  res.send(result);
}

function getUserByIdV2(request,response){
  console.log("GET /apitechu/v1/users/:id");
  console.log("id")
  console.log(request.params.id);
  var httpClient=requestJson.createClient(baseMlabURL);
  var query="q={'id':"+request.params.id+"}"
  httpClient.get('user?'+query+'&'+apiKey, function(err, res, body) {
    if(!err){
      var result={"users":body};
      response.send(result);
    }else{
      response.send( { "mensaje" : "Error Devolviendo Usuarios" })
    }
  });
}

function getUsersV2(request,response){
  console.log("GET /apitechu/v2/users");
  var httpClient=requestJson.createClient(baseMlabURL);
  httpClient.get('user?'+apiKey, function(err, res, body) {
    if(!err){
      var result={"users":body};
      response.send(result);
    }else{
      response.send( { "mensaje" : "Error Devolviendo Usuarios" })
    }
  });
}

function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var new_user={
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email
  };
  console.log("New User:"+new_user);

  var users=require('../usuarios.json');
  users.push(new_user);
  console.log("Usuario añadido al Array");
  io.writeUserDataToFile(users);
}

function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id")
  console.log(req.params.id);
  var id=req.params.id;
  var users=require('../usuarios.json');
  var index=findId_foreach(id);
  console.log("Index:"+index);
  users.splice(index,1);
  io.writeUserDataToFile(users);
}

function findId_foreach(id){
  var users=require('../usuarios.json');
  var index=0;
  users.forEach(function(user) {
    console.log(user);
    if(user.id==id){
      users.splice(index,1);
    }
    index++;
  });
  return -1;
}

module.exports.getUserByIdV2=getUserByIdV2;
module.exports.getUsersV1=getUsersV1;
module.exports.getUsersV2=getUsersV2;
module.exports.createUserV1=createUserV1;
module.exports.deleteUserV1=deleteUserV1;
