const io=require("../io")

function loginV1(req,res){
    email=req.body.email;
    password=req.body.password;
    var users=require('../usuarios.json');
    console.log("User:"+email)
    console.log("Password:"+password);

    index=findUserByMail(email);
    if(index!=-1){
      //Check que password sea igual al que pasamos
      user=users[index];
      console.log(user.email);
      if(user.password==password){
        //Login correcto { "mensaje" : "Login correcto", "idUsuario" : 1 }
        user.logged=true;
        io.writeUserDataToFile(users);
        res.send({"mensaje" : "Login correcto","idUsuario" :""+user.id});
      }else {
        //Login incorrecto { "mensaje" : "Login incorrecto" }
        res.send({ "mensaje" : "Login incorrecto" })
      }
    }else{
      res.send({ "mensaje" : "Login incorrecto" })
    }
}

function logoutV1(req,res){
    id=req.params.id;
    var users=require('../usuarios.json');
    console.log("Id:"+id)
    index=findUserById(id);
    if(index!=-1){
      user=users[index];
      if(user.logged){
        delete user.logged;
        io.writeUserDataToFile(users);
        res.send( { "mensaje" : "logout correcto", "idUsuario" :""+user.id });
      }else{
        res.send( { "mensaje" : "logout incorrecto" })
      }
    }else{
      res.send( { "mensaje" : "logout incorrecto" })
    }
}



function findUserByMail(email){
  var result=-1;
  var users=require('../usuarios.json');
  for (var i = 0; i < users.length; i++){
      user=users[i];
      if(user.email==email){
        console.log("Encontrado");
        result=i;
        break;
      }
  }
  return result;
}

function findUserById(email){
  var result=-1;
  var users=require('../usuarios.json');
  for (var i = 0; i < users.length; i++){
      user=users[i];
      if(user.id==id){
        console.log("Encontrado");
        result=i;
        break;
      }
  }
  return result;
}

module.exports.loginV1=loginV1;
module.exports.logoutV1=logoutV1;
