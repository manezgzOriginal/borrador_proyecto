const express= require('express');
const app=express();
app.use(express.json());
const port=process.env.PORT || 3000;
const io=require("./io")
const userController=require("./controllers/userController.js")
const authController=require("./controllers/authController.js")
app.listen(port);
console.log("Api escuchando en el puertos "+port);

app.get("/apitechu/v1/hello",
    function(req,res){
      console.log("GET /apitechu/v1/hello");
      res.send({"msg":"Hola desde Apitechu"});
    }
);

app.get("/apitechu/v1/users",userController.getUsersV1);
app.get("/apitechu/v2/users",userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUserByIdV2);

app.post("/apitechu/v1/users",userController.createUserV1);

app.post("/apitechu/v1/monstruo/:p1/:p2",
    function(req,res){
      console.log("POST /apitechu/monstruo");
      console.log("Parametros")
      console.log(req.params);
      console.log("Query String");
      console.log(req.query);
      console.log("Headers");
      console.log(req.headers);
      console.log("Body");
      console.log(req.body);

    }
);

app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);


app.post("/apitechu/v1/login",authController.loginV1);

app.post("/apitechu/v1/logout/:id",authController.logoutV1);


function findId_for_normal(id){
  var result=-1;
  var users=require('./usuarios.json');
  for (var i = 0; i < users.length; i++){
      user=users[i];
      if(user.id==id){
        console.log("Encontrado");
        result=i;
        break;
      }
  }
  return result;
}

function findId_foreach(id){
  var users=require('./usuarios.json');
  var index=0;
  users.forEach(function(user) {
    console.log(user);
    if(user.id==id){
      users.splice(index,1);
    }
    index++;
  });
  return -1;
}

function findId_forof(id){
  var result=-1;
  var users=require('./usuarios.json');
  var index=0;
  for (var user of users) {
    if(user.id==id){
      return index;
    }
    index++;
  };
  return -1;
}

function findId_findIndex(id){
  var users=require('./usuarios.json');
  return users.findIndex(x => x.id ==id);
}

function findId_array_entries(id){
  var users=require('./usuarios.json');
  var iterator = users.entries();
  for (var entry of iterator) {
    console.log(entry.next().value)
  }
}
