const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp)

var should = chai.should();

describe ("First test",
  function(){
    it('Test that works',function(done){
      chai.request("http://www.duckduckgo.com")
          .get('/')
          .end(
            function(err,res){
              console.log("Request finished")
              res.should.have.status(200);
              done()
            }
          )
    })
  }
)

describe ("Users API",
  function(){
    it('Test that works',function(done){
      chai.request("localhost:3000")
          .get('/apitechu/v1/users')
          .end(
            function(err,res){
              console.log("Request finished")
              res.should.have.status(200)
              res.body.users.should.be.a("array");
              for(user of res.body.users){
                user.should.have.property("email")
                user.should.have.property("first_name")
              }
              done()
            }
          )
    })
  }
)
